export const swApiBaseUrl = process.env.REACT_APP_SW_API_BASE;
export const peopleUrl = `${swApiBaseUrl}/people`;
export const filmsUrl = `${swApiBaseUrl}/films`;