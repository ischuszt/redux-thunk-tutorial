import React from "react";
import "./App.css";
import { Container, Segment, Header } from "semantic-ui-react";
import Films from "./Films";
import FilmDetails from "./FilmDetails";

function App() {
  return (
    <Container>
      <Segment>
        <Header as="h2">Star Wars API explorer</Header>
        <Films />
        <FilmDetails />
      </Segment>
    </Container>
  );
}

export default App;
