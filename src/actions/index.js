import * as at from "./types";
import { filmsUrl } from "../config";

// Action creators
export const getFilms = () => ({
  type: at.GET_FILMS
});

export const getFilmsSuccess = data => ({
  type: at.GET_FILMS_SUCCESS,
  payload: data
});

// Async action creators
export const fetchFilmData = () => async dispatch => {
  dispatch(getFilms());
  const filmData = await fetch(filmsUrl);
  const data = await filmData.json();
  console.log("film data: ", data);
  dispatch(getFilmsSuccess(data));
};
