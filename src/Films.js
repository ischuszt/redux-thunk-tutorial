import React from "react";
import { Segment, Header, Dimmer, Loader, Table } from "semantic-ui-react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchFilmData } from "./actions";

const Loading = () => (
  <Dimmer active inverted>
    <Loader inverted>Loading</Loader>
  </Dimmer>
);

class Films extends React.Component {
  componentDidMount() {
    const { fetchFilmData } = this.props;
    fetchFilmData();
  }

  selectMovie = movie => {
    // TODO dispatch this
    console.log("Selected: ", movie);
  };

  renderFilms() {
    const { films } = this.props;
    return films.map(f => (
      <Table.Row key={f.title}>
        <Table.Cell>
          <a style={{ cursor: "pointer" }} onClick={() => this.selectMovie(f)}>
            {f.title}
          </a>
        </Table.Cell>
        <Table.Cell>{f.release_date}</Table.Cell>
      </Table.Row>
    ));
  }

  render() {
    const { films, loading } = this.props;
    return (
      <Segment color="red">
        <Header as="h3">Available films</Header>
        {loading && <Loading />}
        {films && films.length > 0 && (
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Title</Table.HeaderCell>
                <Table.HeaderCell>Release Date</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.renderFilms()}</Table.Body>
          </Table>
        )}
      </Segment>
    );
  }
}

const mapStateToProps = state => ({
  films: state.films.data,
  loading: state.films.loading
});

const mapDispatchToProps = {
  fetchFilmData
};

Films.propTypes = {
  films: PropTypes.array.isRequired,
  fetchFilmData: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Films);
