import * as at from "../actions/types";

const initialState = {
  loading: false,
  data: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case at.GET_FILMS:
      return { ...state, loading: true };
    case at.GET_FILMS_SUCCESS:
      return { ...state, loading: false, data: [...action.payload.results] };
    default:
      return state;
  }
};

export default reducer;
