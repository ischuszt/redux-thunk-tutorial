import { combineReducers } from "redux";
import films from "./films";
import people from "./people";

const rootReducer = combineReducers({
  people,
  films
});

export default rootReducer;
